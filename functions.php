<?php


/************************************************
//Unregister some useless widgets 
function my_widgets_init() {
    unregister_widget( 'WP_Widget_Calendar' );
    unregister_widget( 'WP_Widget_Archives' );
    unregister_widget( 'WP_Widget_Meta' );
    unregister_widget( 'WP_Widget_Recent_Comments' );
    unregister_widget( 'WP_Widget_Text' );
    unregister_widget( 'WP_Widget_Pages' );
    unregister_widget( 'WP_Widget_Recent_Posts' );
    unregister_widget( 'WP_Widget_Tag_Cloud' );
}
add_action('widgets_init', 'my_widgets_init');*/


/************************************************
 * Load the custom js functions
 ************************************************/
function uor_scripts($hook) {
 
    // create my own version codes
    $my_js_ver  = date("ymd-Gis", filemtime( get_stylesheet_directory_uri() . '/js/uor_scripts.js' ));
     
    // 
    wp_enqueue_script( 'uor_scripts', get_stylesheet_directory_uri().'/js/uor_scripts.js', array( 'jquery' ), $my_js_ver, true );
 
}
add_action('wp_enqueue_scripts', 'uor_scripts');

/************************************************
 * Add section for cookie notification
 ************************************************/
function cookie_notification() { 
    echo '<div class="cookie-notice" id="cookie-notice">
		  <div class="cookie-notice-container">
			<h3><span>University of Reading </span>cookie policy</span></h3>
			<p>We use cookies on reading.ac.uk to improve your experience. You can find out more about our <a href="http://www.reading.ac.uk/15/about/about-privacy.aspx#cookies">cookie policy</a>.<br>By continuing to use our site you accept these terms, and are happy for us to use cookies to improve your browsing experience.</p>
			<a id="cookie-notice-button" href="#cookie">Continue using this website</a> 
		  </div>
		</div>'; 
}
add_action('wp_footer', 'cookie_notification'); 

/************************************************
//Add pictures to the RSS feeds  */
function featuredtoRSS($content) {
global $post;
if ( has_post_thumbnail( $post->ID ) ){
$content = '<div>' . get_the_post_thumbnail( $post->ID, 'medium', array( 'style' => 'margin-bottom: 15px;' ) ) . '</div>' . $content;
}
return $content;
}
add_filter('the_excerpt_rss', 'featuredtoRSS');
add_filter('the_content_feed', 'featuredtoRSS');

/************************************************
 * Registers an editor stylesheet for the theme.
 */
function wpdocs_theme_add_editor_styles() {
    add_editor_style( './css/custom-editor-style.css' );
}
add_action( 'admin_init', 'wpdocs_theme_add_editor_styles' );

/************************************************
 * Show all parents, regardless of post status. (Fix for private pages)
 * Comes from here: https://www.mightyminnow.com/2014/09/include-privatedraft-pages-in-parent-dropdowns/
 */
function my_slug_show_all_parents( $args ) {
	$args['post_status'] = array( 'publish', 'pending', 'draft', 'private' );
	return $args;
}
add_filter( 'page_attributes_dropdown_pages_args', 'my_slug_show_all_parents' );
add_filter( 'quick_edit_dropdown_pages_args', 'my_slug_show_all_parents' );

/************************************************
 * Remove archive name from page title
 */
add_filter( 'get_the_archive_title', function ($title) {
    if ( is_category() ) {
            $title = single_cat_title( '', false );
        } elseif ( is_tag() ) {
            $title = single_tag_title( '', false );
        } elseif ( is_author() ) {
            $title = '<span class="vcard">' . get_the_author() . '</span>' ;
        }
    return $title;
});
 
 /************************************************
 * Hide the Add New User page
 */
function custom_menu_page_removing() {
    remove_submenu_page( 'users.php','user-new.php' );
}
add_action( 'admin_menu', 'custom_menu_page_removing' );

// Hide button to add user on users list page
function this_screen() {
    $current_screen = get_current_screen();
    if( $current_screen->id === "users" ) {
		echo '<style type="text/css">
		.page-title-action { display:none; }
		</style>';
    }
}
add_action( 'current_screen', 'this_screen' );

/************************************************
 * Change admin footer to link to the help page
 */
function change_admin_footer(){
	 echo '<span id="footer-note" style="color:red;"><a href="https://research.reading.ac.uk/act/knowledgebase_category/web/" target="_blank" style="color:red;">Help for University of Reading users</a></span>';
	}
add_filter('admin_footer_text', 'change_admin_footer');

/************************************************
 * Add content within shortcodes to the excerpt
 */
add_filter('the_excerpt', 'do_shortcode');
remove_filter('get_the_excerpt', 'wp_trim_excerpt', 10);
add_filter('get_the_excerpt', 'my_custom_wp_trim_excerpt', 99, 1);
function my_custom_wp_trim_excerpt($text) {
    if(''==$text) {
        $text= preg_replace('/\s/', ' ', wp_strip_all_tags(get_the_content('')));
        $text= explode(' ', $text, 56);
        array_pop($text);
        $text= implode(' ', $text);
    }
    return $text;
}
add_filter( 'the_excerpt', 'shortcode_unautop');
add_filter( 'the_excerpt', 'do_shortcode');
add_filter('get_the_excerpt', 'shortcode_unautop');
add_filter('get_the_excerpt', 'do_shortcode');

/************************************************
 * Highlight the search results in the result page
 */
function wps_highlight_results($text){
	if( is_search() && ( !is_admin() ) ){ //Do not add the html tag in the admin interface as it won't be interpreted
		$sr = get_query_var('s');
		$keys = explode(" ",$sr);
		$text = preg_replace('/('.implode('|', $keys) .')/iu', '<text style="background-color: yellow;">'.$sr.'</text>', $text);
	}
	return $text;
}
add_filter('the_excerpt', 'wps_highlight_results');
add_filter('the_title', 'wps_highlight_results');

/************************************************
// This is PHP function to convert a user-supplied URL to just the domain name,
// which I use as the link text. (https://gist.github.com/davejamesmiller/1965937)
// Remember you still need to use htmlspecialchars() or similar to escape the
// result. */
function url_to_domain($url)
{
    $host = @parse_url($url, PHP_URL_HOST);

    // If the URL can't be parsed, use the original URL
    // Change to "return false" if you don't want that
    if (!$host)
        $host = $url;

    // The "www." prefix isn't really needed if you're just using
    // this to display the domain to the user
    if (substr($host, 0, 4) == "www.")
        $host = substr($host, 4);

    return $host;
}
//Function to check that the provided URL for external page load is from the University domain. If not, it is not loaded.
function include_external_page ($pageURL){
	if (substr(url_to_domain($pageURL), -13)!='reading.ac.uk'){
		echo "This page is trying to load an unauthorized external resource...";
	}
	else {
		$headers = @get_headers($pageURL);
		if ( strpos($headers[0],'200') ) //If header shows that website is accessible (http code 200), we'll load it.
		{
			echo wp_remote_fopen($pageURL);
		}
		else
		{
			echo "ERROR: The external content cannot be loaded (Trying to load: ".$pageURL.")";
			echo "<br> Code Error: ".$headers[0];
		}		
	}
}

?>