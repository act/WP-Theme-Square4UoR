/*************************************************
  Hide sub-menu under university logo and add link on banner
**************************************************/
jQuery(function($){
	$("#univ_links").hide();
	$("#sq-site-branding").mouseenter(function(){
		$("#univ_links").show(200);
	});
	$("#sq-site-branding").mouseleave(function(){
		$("#univ_links").hide(200);
	});
	$(".sq-main-header").click(function(){
		$( location ).attr("href", siteurl);
	});
});

/*************************************************
  Change Knowledge Base search field text
**************************************************/
jQuery(function($){
	$("#searchform input:text").attr("value", "Search...");
	$("#searchform input:text").attr("onblur", "if (this.value == '')  {this.value = 'Search...';}");
	$("#searchform input:text").attr("onfocus", "if (this.value == 'Search...') {this.value = '';}");
});


/*************************************************
  Cookie notification banner
**************************************************/
jQuery(function($){
	$(document).ready(function()
	{
		/******************
		COOKIE NOTICE
		******************/
		if(getCookie('show_cookie_message') != 'no')
		{
			$('#cookie-notice').show();
		}

		$('#cookie-notice-button').click(function()
		{
			$('#cookie-notice').animate({opacity:0 }, "slow");
			setCookie('show_cookie_message','no');
			return false;
		});
	});
});

function setCookie(cookie_name, value)
{
    var exdate = new Date();
		console.log(cookie_name);
    exdate.setDate(exdate.getDate() + (365*5));
    document.cookie = cookie_name + "=" + escape(value) + "; expires="+exdate.toUTCString() + "; path=/";
}

function getCookie(cookie_name)
{
    if (document.cookie.length>0)
    {
        cookie_start = document.cookie.indexOf(cookie_name + "=");
        if (cookie_start != -1)
        {
            cookie_start = cookie_start + cookie_name.length+1;
            cookie_end = document.cookie.indexOf(";",cookie_start);
            if (cookie_end == -1)
            {
                cookie_end = document.cookie.length;
            }
            return unescape(document.cookie.substring(cookie_start,cookie_end));
        }
    }
    return "";
}
