<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package Square
 */

get_header(); ?>

<header class="sq-main-header">
	<div class="sq-container">
		<h1 class="sq-main-title"><?php esc_html_e( 'Page not accessible', 'square' ); ?></h1>
	</div>
</header><!-- .entry-header -->

<div class="sq-container">

	<p><?php esc_html_e( 'That page can&rsquo;t be found, or is not accessible.', 'square' ); ?></p>
	<br><br>
	<?php
	if ( !is_user_logged_in() ) {
		echo '<p>If you\'re trying to access a protected page, you must first <a href=\'./wp-login.php\'><b>login</b>.</a></p>';
	} 
	?>

</div>

<?php get_footer(); ?>
