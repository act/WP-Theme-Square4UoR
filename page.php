<?php
/**
 * The template for displaying all pages.
 *
 * @package Square
 */

get_header(); ?>
<style>
	.kbe_home {
		background-color:<?php echo KBE_BG_COLOR;?> !important;
	}
</style>

<header class="sq-main-header">
	<div class="sq-container">
		<?php the_title( '<h1 class="sq-main-title">', '</h1>' ); ?>
	</div>
</header><!-- .entry-header -->

<div class="sq-container sq-clearfix">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'template-parts/content', 'page' ); ?>

				<?php
					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;
				?>

			<?php endwhile; // End of the loop. 
			//include external page if any
			if (get_field('include_page')!="") {//echo file_get_contents(get_field('include_page')); 
				include_external_page (get_field('include_page'));
			}
			?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
</div>

<?php get_footer(); ?>

