# WP child theme Square for UoR

The development of this Wordpress child theme has been part of the broader "Wordpress for Research" project, in Spring 2017.
This child theme adds a university logo on top left, with internal links. It also adds custom fields, such as external page inclusion. 
The child theme also contains a cookie notification banner at the bottom.
A few css modifications have been applied to comply with the University communication guidelines.

Parent theme: https://hashthemes.com/wordpress-theme/square/ 


### Prerequisites

Wordpress 4.8 and above

```
Example: https://research.reading.ac.uk/met-spate/
```

### Installing

Make sure to have the square theme installed.
Simply copy the files of this child theme into the directory ...\wp-content\themes\Square4UoR
Activate the theme in the Wordpress network dashboard.


## Built With

The structure and scripts used in this theme are copied from the original Active-Edition template.

## Contributing

UoR internal contribution only

## Versioning

April 2019: V2.0 (load uor logo in header, enqueue scripts with WP function...)

## Authors

University of Reading / Academic Computing Team / Eric Mathieu

## License

All developments are the property of the University of Reading.
The parent theme has its own licence rules. Check at: https://hashthemes.com/wordpress-theme/square/ 
No copy authorized.
