<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

get_header( 'knowledgebase' );

?>

<!-- Had to hard code the following to comply with square theme and display the banner picture: -->
<header class="sq-main-header">
	<div class="sq-container">
		<h1 class="sq-main-title">Knowledge Base</h1>
	</div>
</header>
<!-- end addition to comply with square theme -->
<style>
	.kbe_category {
		background-color:<?php echo KBE_BG_COLOR;?> !important;
	}
	.kbe_leftcol_list_articles {
		border-color: <?php echo KBE_BG_COLOR;?> !important;
	}
	button.kbe_subcataccordion, button.kbe_subcataccordion:before, button.kbe_subcataccordion.active:before {
		color: <?php echo KBE_BG_COLOR;?> !important;
	}
</style>
<?php
// load the style and script
wp_enqueue_style( 'kbe_theme_style' );
if ( KBE_SEARCH_SETTING == 1 ) {
	wp_enqueue_script( 'kbe_live_search' );
}

// Classes For main content div
if ( KBE_SIDEBAR_INNER == 0 ) {
	$kbe_content_class = 'class="kbe_content_full"';
} elseif ( KBE_SIDEBAR_INNER == 1 ) {
	$kbe_content_class = 'class="kbe_content_right"';
} elseif ( KBE_SIDEBAR_INNER == 2 ) {
	$kbe_content_class = 'class="kbe_content_left"';
}

// Classes For sidebar div
if ( KBE_SIDEBAR_INNER == 0 ) {
	$kbe_sidebar_class = 'kbe_aside_none';
} elseif ( KBE_SIDEBAR_INNER == 1 ) {
	$kbe_sidebar_class = 'kbe_aside_left';
} elseif ( KBE_SIDEBAR_INNER == 2 ) {
	$kbe_sidebar_class = 'kbe_aside_right';
}

// Query for the Category
$kbe_cat_slug = get_queried_object()->slug;
$kbe_cat_name = get_queried_object()->name;
$kbe_cat_desc = get_queried_object()->description;
$kbe_cat_id = get_queried_object()->term_id;
$kbe_cat_parent = get_queried_object()->parent;
if ( $kbe_cat_parent != 0 ) $kbe_parent_term_name = get_the_category_by_ID( $kbe_cat_parent );
else $kbe_parent_term_name = "";

//Query for this cat articles
$kbe_tax_post_args = array(
	'post_type'      => KBE_POST_TYPE,
	'posts_per_page' => 999,
	'orderby'        => 'menu_order',
	'order'          => 'ASC',
	'tax_query'      => array(
		array(
			'taxonomy' => KBE_POST_TAXONOMY,
			'field'    => 'slug',
			'terms'    => $kbe_cat_slug,
			'include_children' => false
		)
	)
);
$kbe_tax_post_qry  = new WP_Query( $kbe_tax_post_args );


?><div id="kbe_container"><?php

	// Breadcrumbs
	if ( KBE_BREADCRUMBS_SETTING == 1 ) {
		?><div class="kbe_breadcrum"><?php
			kbe_breadcrumbs();
		?></div><?php
	}

	// Search field
	if ( KBE_SEARCH_SETTING == 1 ) {
		kbe_search_form();
	}

	// Content
	?><div id="kbe_content" <?php echo $kbe_content_class; ?>>
        <!--leftcol-->
        <div class="kbe_leftcol kbe_leftcol_list_articles">
            <!--<articles>-->
				<div class="kbe_category kbe_category_list_articles" >
					<?php
						echo "<h2>";
						if( class_exists('acf') ){
							if (get_field('kb_cat_featured_picture','kbe_taxonomy_'.$kbe_cat_id)!="") {
									$image =  get_field('kb_cat_featured_picture','kbe_taxonomy_'.$kbe_cat_id);
									echo "<img src=\"".$image['url']."\" alt=\"".$image['alt']."\">";
							}
						} 
						if ( $kbe_parent_term_name != "" ) echo $kbe_parent_term_name.": ".$kbe_cat_name."</h2>";
						else echo $kbe_cat_name."</h2>";
					?>	
				</div>
				<?php
					if ( $kbe_tax_post_qry->have_posts() ) :
						?><div class="kbe_articles"><ul><?php
						while ( $kbe_tax_post_qry->have_posts() ) :
							$kbe_tax_post_qry->the_post();
							?><li>
                                <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(array(40,40));the_title(); ?></a>
                            </li><?php
						endwhile;
						?></ul></div><?php
					endif;
				?>
				

				<?php
				$kbe_child_cat_args = array(
						'orderby'    => 'terms_order',
						'order'      => 'ASC',
						'parent'     => $kbe_cat_id,
						'hide_empty' => true,
					);
				$kbe_child_terms = get_terms( KBE_POST_TAXONOMY, $kbe_child_cat_args );

				if ( $kbe_child_terms ) {
					foreach ( $kbe_child_terms as $kbe_child_term ) {
						$kbe_child_term_id   = $kbe_child_term->term_id;
						$kbe_child_term_slug = $kbe_child_term->slug;
						$kbe_child_term_name = $kbe_child_term->name;
						?>
						<button class="kbe_subcataccordion"><?php 
							if( class_exists('acf') ){ //Display featured picture if any
								if (get_field('kb_cat_featured_picture','kbe_taxonomy_'.$kbe_child_term_id)!="") {
										$image =  get_field('kb_cat_featured_picture','kbe_taxonomy_'.$kbe_child_term_id);
										echo "<img src=\"".$image['url']."\" alt=\"".$image['alt']."\" style=\"max-height:40px\">";
								}
							} 
							echo $kbe_child_term_name;?>
						</button>
						<div class="kbe_panel">
							<div class="kbe_articles">	
								<?php
								$kbe_child_post_args = array(
									'post_type'      => KBE_POST_TYPE,
									'orderby'        => 'menu_order',
									'order'          => 'ASC',
									'posts_per_page' => 999,
									'tax_query'      => array(
										array(
											'taxonomy' => KBE_POST_TAXONOMY,
											'field'    => 'term_id',
											'terms'    => $kbe_child_term_id
										)
									)
								);

								$kbe_child_post_qry = new WP_Query( $kbe_child_post_args );
								?><ul>
								<?php
								if ( $kbe_child_post_qry->have_posts() ) :
									while ( $kbe_child_post_qry->have_posts() ) :
										$kbe_child_post_qry->the_post();

										?><li>
											<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(array(40,40)); the_title(); ?></a>
										</li><?php

									endwhile;
								else :
									echo 'No posts';
								endif;
								?>
								</ul>
							
							

							</div>
						</div>
					<?php 
					}
				}?>
        </div>
        <!--/leftcol-->

    </div>
    <!--/content-->

    <!--aside-->
    <div class="kbe_aside <?php echo $kbe_sidebar_class; ?>"><?php
		if ( (KBE_SIDEBAR_INNER == 2) || (KBE_SIDEBAR_INNER == 1) ) {
			dynamic_sidebar( 'kbe_cat_widget' );
		}
	?></div>

</div>


<script>
var acc = document.getElementsByClassName("kbe_subcataccordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].onclick = function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    } 
  }
}
</script>

<?php
get_footer( 'knowledgebase' );
