<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

get_header( 'knowledgebase' );

// load the style and script
wp_enqueue_style( 'kbe_theme_style' );
if ( KBE_SEARCH_SETTING == 1 ) {
	wp_enqueue_script( 'kbe_live_search' );
}

// Classes For main content div
if ( KBE_SIDEBAR_INNER == 0 ) {
	$kbe_content_class = 'class="kbe_content_full"';
} elseif ( KBE_SIDEBAR_INNER == 1 ) {
	$kbe_content_class = 'class="kbe_content_right"';
} elseif ( KBE_SIDEBAR_INNER == 2 ) {
	$kbe_content_class = 'class="kbe_content_left"';
}

// Classes For sidebar div
if ( KBE_SIDEBAR_INNER == 0 ) {
	$kbe_sidebar_class = 'kbe_aside_none';
} elseif ( KBE_SIDEBAR_INNER == 1 ) {
	$kbe_sidebar_class = 'kbe_aside_left';
} elseif ( KBE_SIDEBAR_INNER == 2 ) {
	$kbe_sidebar_class = 'kbe_aside_right';
}
?>
<!-- Had to hard code the following to comply with square theme and display the banner picture: -->
<header class="sq-main-header">
	<div class="sq-container">
		<h1 class="sq-main-title">Knowledge Base</h1>
	</div>
</header>
<!-- end addition to comply with square theme -->
<div id="kbe_container"><?php

	// Breadcrumbs
	if ( KBE_BREADCRUMBS_SETTING == 1 ) {
		?><div class="kbe_breadcrum"><?php
				kbe_breadcrumbs();
		?></div><?php
	}

	// Search field
	if ( KBE_SEARCH_SETTING == 1 ) {
		kbe_search_form();
	}

	// Content
	?><div id="kbe_content" <?php echo $kbe_content_class; ?>>
        <!--Content Body-->
        <div class="kbe_leftcol" ><?php

			while ( have_posts() ) :
				the_post();

				//  Never ever delete it !!!
				kbe_set_post_views( get_the_ID() );
				?><h1><?php 
					the_post_thumbnail(array(50,50)); the_title(); ?></h1><?php

				the_content();

				$current_category = get_the_terms( get_the_ID(), KBE_POST_TAXONOMY ); //Used for display of related articles below
				$current_article = get_the_ID(); //get current article ID, so we don't display it in related articles below
				//include 'kbe_comments.php';
			endwhile;

		?></div>
        <!--/Content Body-->

    </div>

    <!--aside-->
    <div class="kbe_aside <?php echo $kbe_sidebar_class; ?>"><?php
		if ( (KBE_SIDEBAR_INNER == 2) || (KBE_SIDEBAR_INNER == 1) ) {
			dynamic_sidebar( 'kbe_cat_widget' );
		}
	?>
		       
			<?php //Display a link to other articles which are in the same category
			//var_dump($current_category);
			$args = array(	
						'orderby'        => 'menu_order',
						'order'          => 'ASC',
						'post_type'      => 'kbe_knowledgebase',
						'posts_per_page' => '10',
						'post__not_in' => array( $current_article, ),
						'tax_query'      => array(
							array(
								'taxonomy' => KBE_POST_TAXONOMY,
								'field'    => 'slug',
								'terms'    => $current_category[0] -> slug
							)
						)
				);
	 
			// Custom query.
			$query = new WP_Query( $args );
			 
			// Check that we have query results.
			if ( $query->have_posts() ) { ?>
				<div class="kbe_widget kbe_widget_article"><h2>Related articles</h2>  
				<?php
				// Start looping over the query results.
				while ( $query->have_posts() ) {
					$query->the_post(); ?>
                        <p><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(array(30,30)); the_title(); ?></a></p>
					<?php
					// Contents of the queried post results go here.
			 
				}
				?></div><?php
			}
			// Restore original post data.
			wp_reset_postdata();?>
	</div>

</div><?php
get_footer( 'knowledgebase' );
