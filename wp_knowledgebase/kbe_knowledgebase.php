<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/*=========
Template Name: KBE
=========*/

global $wpdb;

get_header( 'knowledgebase' );
?>
<!-- Had to hard code the following to comply with square theme and display the banner picture: -->
<header class="sq-main-header">
	<div class="sq-container">
		<h1 class="sq-main-title">Knowledge Base</h1>
	</div>
</header>
<!-- end addition to comply with square theme -->

<style>
	.kbe_content {
		background-color:<?php echo KBE_BG_COLOR;?> !important;
	}
	.kbe_howto_article {
		color:<?php echo KBE_BG_COLOR;?> !important;
	}
</style>

<?php
//PHP function to display the tree view (uses a loop counter $i to prevent any infinite loop...
function loop_tree_view($table,$i){
	foreach ($table as $cat_name => $table_rest) { //loop on top category
		if ( ( is_int( $cat_name ) ) || ( !is_array( $table_rest ) ) || ( $i > 500 ) ){
			echo "<li class=\"kbe_list_article_title\"><a href=\"".$table_rest["link"]."\">".$table_rest["title"]."</a></li>";
		}
		else {
			echo "<li><span class=\"caret\">".$cat_name."</span><ul class=\"nested\">";
			$i++;
			loop_tree_view($table_rest,$i);
			echo "</ul></li>";
		}
	}
}


// load the style and script
wp_enqueue_style( 'kbe_theme_style', '', array(), getdate()[0] );
if ( KBE_SEARCH_SETTING == 1 ) {
	wp_enqueue_script( 'kbe_live_search' );
}

// Classes For main content div
if ( KBE_SIDEBAR_HOME == 0 ) {
	$kbe_content_class = 'class="kbe_content_full"';
} elseif ( KBE_SIDEBAR_HOME == 1 ) {
	$kbe_content_class = 'class="kbe_content_right"';
} elseif ( KBE_SIDEBAR_HOME == 2 ) {
	$kbe_content_class = 'class="kbe_content_left"';
}

// Classes For sidebar div
if ( KBE_SIDEBAR_HOME == 0 ) {
	$kbe_sidebar_class = 'kbe_aside_none';
} elseif ( KBE_SIDEBAR_HOME == 1 ) {
	$kbe_sidebar_class = 'kbe_aside_left';
} elseif ( KBE_SIDEBAR_HOME == 2 ) {
	$kbe_sidebar_class = 'kbe_aside_right';
}

// Breadcrumbs
?><div id="kbe_container" class="kbe_home_container"><?php

	if ( KBE_BREADCRUMBS_SETTING == 1 ) {
		?><div class="kbe_breadcrum"><?php
			kbe_breadcrumbs();
		?></div><?php
	}

	// Search field
	if ( KBE_SEARCH_SETTING == 1 ) {
		kbe_search_form();
	} ?>

	<?php
	// Content
	?><div id="kbe_content" <?php echo $kbe_content_class; ?>>
	
		<!--tab icons-->
		<div id="kbe_display_list"><a href="javascript:void(0)" onclick="openTab('kbe_tab_tree_view');"><img src="<?php echo get_stylesheet_directory_uri()."/list-display.png"; ?>" width="40px" height="40px"></a></div>
		<div id="kbe_display_box"><a href="javascript:void(0)" onclick="openTab('kbe_tab_box_view');"><img src="<?php echo get_stylesheet_directory_uri()."/box-display.png"; ?>" width="40px" height="40px"></a></div>
	
	
        <!--<h1><?php echo get_the_title( KBE_PAGE_TITLE ); ?></h1>-->
		

        <!--leftcol-->
        <div class="kbe_leftcol">
			<div id="kbe_tab_box_view" class="kbe_categories tab"  style="display:none"><?php

			$kbe_cat_args = array(
				'orderby'    => 'terms_order',
				'order'      => 'ASC',
				'hide_empty' => true,
				'parent'     => 0
			);

			$kbe_terms = get_terms( KBE_POST_TAXONOMY, $kbe_cat_args );
			$i = 0;
			foreach ( $kbe_terms as $kbe_taxonomy ) 
			{
				$kbe_term_id   = $kbe_taxonomy->term_id;
				$kbe_term_slug = $kbe_taxonomy->slug;
				$kbe_term_name = $kbe_taxonomy->name;
				$kbe_term_desc = $kbe_taxonomy->description;

				$kbe_taxonomy_parent_count = $kbe_taxonomy->count;

					$children = get_term_children( $kbe_term_id, KBE_POST_TAXONOMY );

					$kbe_count_sum = $wpdb->get_var( "
						SELECT Sum(count)
						FROM {$wpdb->prefix}term_taxonomy
						WHERE taxonomy = '" . KBE_POST_TAXONOMY . "'
						And parent = $kbe_term_id
					" );

					$kbe_count_sum_parent = '';

					if ( $children ) {
						$kbe_count_sum_parent = $kbe_count_sum + $kbe_taxonomy_parent_count;
					} else {
						$kbe_count_sum_parent = $kbe_taxonomy_parent_count;
					}

				?>
				
				<span class="kbe_category <?php if ( $kbe_term_slug == "howto" ) echo " kbe_howto_cat";?>">
					<div class="kbe_content" id="kbe_content_<?php echo $i; ?>">

						<!--
						Link disabled to  allow toggle of kbe how to articles -  see also line 335 -->
						<a href="<?php /*echo get_term_link( $kbe_term_slug, 'kbe_taxonomy' );*/ ?>" >
						
						<h2>
								<?php
									if( class_exists('acf') ){
										if (get_field('kb_cat_featured_picture','kbe_taxonomy_'.$kbe_term_id)!="") {
											$image =  get_field('kb_cat_featured_picture','kbe_taxonomy_'.$kbe_term_id);
											echo "<img src=\"".$image['url']."\" alt=\"".$image['alt']."\">";
										}
									} 
									echo $kbe_term_name; ?>
							</h2>
						</a>
						<?php ?>
						<?php
						if ( $kbe_term_desc <> "" ) echo "<div id='kb_cat_desc_".$i."' class='kb_cat_description'><a href='".get_term_link( $kbe_term_slug, 'kbe_taxonomy' )."'>".$kbe_term_desc."</a></div>"; //display the category description in a hidden div that appears on mouseover
						$kbe_child_cat_args = array(
							'orderby'    => 'terms_order',
							'order'      => 'ASC',
							'parent'     => $kbe_term_id,
							'hide_empty' => true,
						);

						$kbe_child_terms = get_terms( KBE_POST_TAXONOMY, $kbe_child_cat_args );

						if ( $kbe_child_terms ) {
							foreach ( $kbe_child_terms as $kbe_child_term ) {
								$kbe_child_term_id   = $kbe_child_term->term_id;
								$kbe_child_term_slug = $kbe_child_term->slug;
								$kbe_child_term_name = $kbe_child_term->name;
								$kbe_child_term_nb_articles = $kbe_child_term->count;
								
								$kbe_child_post_args = array(
									'post_type'      => KBE_POST_TYPE,
									'tax_query'      => array(
										array(
											'taxonomy' => KBE_POST_TAXONOMY,
											'field'    => 'term_id',
											'terms'    => $kbe_child_term_id
										)
									)
								);
								$kbe_child_post_qry = new WP_Query( $kbe_child_post_args );
								
							
								//If only one article in the sub category, we'll link directly to iterator_apply
								if ($kbe_child_term_nb_articles == 1 ){
									if ( $kbe_child_post_qry->have_posts() ) :
										while ( $kbe_child_post_qry->have_posts() ) :
											$kbe_child_post_qry->the_post();
											/*Child categories are not displayed anymore since 20/03/2019 ?>
											<a href="<?php the_permalink(); ?>"><div class="kbe_child_category" ><?php
												echo $kbe_child_term_name;
											?></div></a>
											<?php*/
										endwhile;
									else :
										echo 'No posts';
									endif;
								}
								else { //More than 1 article, we link to the category page:
									/*Child categories are not displayed anymore since 20/03/2019 ?>
									<a href="<?php echo get_term_link( $kbe_child_term_slug, 'kbe_taxonomy' ); ?>"><div class="kbe_child_category" ><?php
										echo $kbe_child_term_name;
									?></div></a>
									<?php */
								}
								// BUILD TREE VIEW: ADD ARTICLES OF SUB CATEGORIES
								while ( $kbe_child_post_qry->have_posts() ) :
									$kbe_child_post_qry->the_post();
									$list_articles [$kbe_term_name][$kbe_child_term_name][get_the_ID()]["link"] = get_the_permalink();
									$list_articles [$kbe_term_name][$kbe_child_term_name][get_the_ID()]["title"] = get_the_title();
								endwhile;
							}
						}

								
						// BUILD THE TREE VIEW: SEARCH ARTICLES ATTACHED TO TOP CATEGORY
						$kbe_tax_post_args = array(
							'post_type'      => KBE_POST_TYPE,
							'orderby'        => 'menu_order',
							'order'          => 'ASC',
							'tax_query'      => array(
								array(
									'taxonomy' => KBE_POST_TAXONOMY,
									'field'    => 'slug',
									//'terms'    => $kbe_child_term_slug,
									'terms'    => $kbe_term_slug,
									'include_children' => false
								)
							)
						);

						$kbe_tax_post_qry = new WP_Query( $kbe_tax_post_args );

						if ( $kbe_tax_post_qry->have_posts() ) :
							?>
							<div id="cat_articles">
							<?php
							while ( $kbe_tax_post_qry->have_posts() ) :
								$kbe_tax_post_qry->the_post();

								$list_articles [$kbe_term_name][get_the_ID()]["link"] = get_the_permalink();
								$list_articles [$kbe_term_name][get_the_ID()]["title"] = get_the_title(); 	
								
								// IF DISPLAYING THE HOWTO CATEGORY, WE ADD THE ARTICLES DIRECTLY
								if ( $kbe_term_slug == "howto" ){ ?>
									<a href="<?php the_permalink(); ?>"><div class="kbe_howto_article" ><?php
											 the_post_thumbnail(array(40,40)); the_title();
										?></div>
									</a> <?php 
								} 

							endwhile;
							?>
							</div>
							<?php
						endif; ?>

					</div>
				</span>
			<?php $i++;
			} ?>
			</div>
			<div id="kbe_tab_tree_view" class="tab" style="display:none">
				<ul id="kbe_tree_view">
					<?php
					//echo '<pre>' , var_dump($list_articles) , '</pre>';
					echo "<h2>Tree navigation</h2>";
					loop_tree_view($list_articles,1); /*
					foreach ($list_articles as $cat_name => $cat_level_1) { //loop on top category
						echo "<li><span class=\"caret\">".$cat_name."</span><ul class=\"nested\">";
						foreach ($cat_level_1 as $name_level2 => $cat_level_2){  //loop on sub-category, or aricles in top category
							if ( is_int( $name_level2 ) ){
								echo "<li class=\"kbe_list_article_title\"><a href=\"".$cat_level_2["link"]."\">".$cat_level_2["title"]."</a></li>";
							}
							else {
								echo "<li><span class=\"caret\">".$name_level2."</span><ul class=\"nested\">";
								foreach ($cat_level_2 as $name_level3 => $cat_level_3){ //loop on articles in sub-category or top category article elements
									echo "<li class=\"kbe_list_article_title\"><a href=\"".$cat_level_3["link"]."\">".$cat_level_3["title"]."</a></li>";
								}
								echo "</ul></li>";
							}
						}
						echo "</ul></li>";
					} */
					
					?>
				</ul>
			</div>
        </div>
        <!--/leftcol-->

    </div>
    <!--content-->

    <!--aside-->
    <div class="kbe_aside <?php echo $kbe_sidebar_class; ?>"><?php

		if ( (KBE_SIDEBAR_HOME == 2) || (KBE_SIDEBAR_HOME == 1) ) {
			dynamic_sidebar( 'kbe_cat_widget' );
		}
		
	?></div>
    <!--/aside-->
	
	<script>
	//Script to open and close tabs
	function openTab(tabName) {
	  var i, x;
	  x = document.getElementsByClassName("tab");
	  for (i = 0; i < x.length; i++) {
		 x[i].style.display = "none";
	  }
	  document.getElementById(tabName).style.display = "flex";
	  setCookie('display_options',tabName);
	}
	</script>
	<script>
	//Script to open and close tree view
	var toggler = document.getElementsByClassName("caret");
	var i;

	for (i = 0; i < toggler.length; i++) {
	  toggler[i].addEventListener("click", function() {
		this.parentElement.querySelector(".nested").classList.toggle("active");
		this.classList.toggle("caret-down");
	  });
	}
	</script>

<!--
 	<script>
	//Script to open and close howto articles see also line 140
		jQuery(function($){
			$(document).ready(function(){
			  $(".kbe_howto_cat").click(function(){
				$("#cat_articles").toggle();
			  });
			});
		});
	</script>
-->

 	<script>
	// OLD Script to open and close howto articles
	jQuery(function($){
		$(document).ready(function(){
		  $(".kbe_howto_cat").hover(function(){
			$( "#cat_articles" ).show();
		  },
		  function(){
			$( "#cat_articles" ).hide();
		  }); 
		});
	});
	</script>

	<script>
	// Read the display choice in a cookie. If not set, we'll display the boxes by default
	jQuery(function($){
		$(document).ready(function()
		{
			/******************
			TAB COOKIE READING
			******************/
			if(getCookie('display_options') != '')
			{
				openTab(getCookie('display_options'));
			}
			else openTab('kbe_tab_box_view'); //cookie value not set, we'll display the box view
		});
	});
	</script>

</div><?php
get_footer( 'knowledgebase' );
