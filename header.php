<?php
/**
 * The header for our theme.
 *
 * @package Square
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<script>
	var siteurl = <?php  echo "\"".site_url()."\""; ?>; //URL of the homepage, used later for link on the banenr for instance
</script>
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="sq-page">
	<?php 
	$square_header_bg = get_theme_mod( 'square_header_bg','sq-black' ); 
	$square_sticky_header = get_theme_mod( 'square_disable_sticky_header' ); 
	$square_sticky_header_class = ($square_sticky_header) ? ' disable-sticky' : '';
	?>
	<header id="sq-masthead" class="sq-site-header <?php echo esc_attr($square_header_bg.$square_sticky_header_class); ?>">
		<div class="sq-container sq-clearfix">
			<div id="sq-site-branding" >
				<div id="univ_logo"></div>
				<div id="univ_links"><a href="http://reading.ac.uk/ready-to-study.aspx" >Study &amp; Life</a> | <a href="http://reading.ac.uk/research.aspx">Research</a> | <a href="http://reading.ac.uk/about.aspx">About Us</a></div>
			</div><!-- .site-branding -->

			<div class="sq-toggle-nav">
				<span></span>
			</div>
			
			<nav id="sq-site-navigation" class="sq-main-navigation">
				<?php 
				wp_nav_menu( array( 
					'theme_location' => 'primary', 
					'container_class' => 'sq-menu sq-clearfix' ,
					'menu_class' => 'sq-clearfix',
					'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
				) ); 
				?>
			</nav><!-- #site-navigation -->
		</div>
	</header><!-- #masthead -->

	<div id="sq-content" class="sq-site-content sq-clearfix">
